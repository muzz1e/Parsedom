package dao;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public  class  FilesDao {
    public static ArrayList<String> readFile(String name){
        ArrayList<String> file = new ArrayList<String>();
        try {
                BufferedReader reader = new BufferedReader(new FileReader(name));
                while (reader.ready()){
                    file.add(reader.readLine());
                }
                reader.close();

            } catch (IOException e) {
                    e.printStackTrace();
                }
        return file;
    }

    public static void writeFileEcxel(String fileName, HashMap<String, ArrayList<String>> hashMap) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName));
        for(Map.Entry<String, ArrayList<String>> entry : hashMap.entrySet()){
            for (String text : entry.getValue()) {
                bufferedWriter.write(entry.getKey() + "," + text);
                bufferedWriter.newLine();
            }
        }
        bufferedWriter.close();
    }

}
