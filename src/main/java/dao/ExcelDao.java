package dao;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ExcelDao {

    public static void writeResult(String fileName,HashMap<String, HashMap<String, ArrayList<String>>> hashMap) throws IOException {
        Workbook book = addAllSheet(new HSSFWorkbook(),  hashMap);
        book.write(new FileOutputStream(fileName));
        book.close();
    }

    private static Workbook addSheet(Workbook book, String nameSheet, HashMap<String,  ArrayList<String>> hashMap){
       Sheet sheet =  book.createSheet(nameSheet);
       for (Map.Entry<String, ArrayList<String>> entry : hashMap.entrySet()){
           ArrayList<String> tmpValue = entry.getValue();
           for (int i = 0; i < tmpValue.size() ; i++) {
               Row row = sheet.createRow(i);
               row.createCell(0).setCellValue(entry.getKey());
               row.createCell(1).setCellValue(tmpValue.get(i));
           }
       }
       return book;
    }
    private static Workbook addAllSheet(Workbook book, HashMap<String, HashMap<String, ArrayList<String>>> hashMapAll){
        for (Map.Entry<String, HashMap<String, ArrayList<String>>> hashMap : hashMapAll.entrySet()){
            for (Map.Entry<String, ArrayList<String>> entry : hashMap.getValue().entrySet()){
                Sheet sheet =  book.getSheet(entry.getKey()) == null ? book.createSheet(entry.getKey()) : book.getSheet(entry.getKey());
                ArrayList<String> tmpValue = entry.getValue();
                for (int i = 0; i < tmpValue.size() ; i++) {
                    Row row = sheet.createRow(sheet.getLastRowNum() + 1);
                    row.createCell(0).setCellValue(hashMap.getKey());
                    row.createCell(1).setCellValue(tmpValue.get(i));
                }
            }
        }

        return book;
    }

}
