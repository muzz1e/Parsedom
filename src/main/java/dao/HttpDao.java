package dao;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

public class HttpDao {
    public static Document getDocument(String url) throws IOException {
        Document doc = Jsoup.connect(url).get();
        return doc;
    }
}
