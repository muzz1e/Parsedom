package service.sitemap;

import dao.FilesDao;
import model.SiteMap;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class SiteMapServiceImpl implements SiteMapService {
    private static final String URL_MATCHER = ".*<loc>(.*)</loc>.*";

    @Override
    public SiteMap getUrlsByXml(String fileName, ArrayList<String> tags) {
        ArrayList<String> fileSiteMap = FilesDao.readFile(fileName);
        ArrayList<String> urls = new ArrayList<>();
        Pattern pattern = Pattern.compile(URL_MATCHER);
        for (String line : fileSiteMap) {
            try {
                Matcher matcher = pattern.matcher(line);
                matcher.find();
                urls.add(matcher.group(1));
            }
           catch (Exception e){
           }
        }
        
        return new SiteMap(urls, tags);
    }
}
