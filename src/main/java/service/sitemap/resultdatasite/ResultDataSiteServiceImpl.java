package service.sitemap.resultdatasite;

import dao.HttpDao;
import model.ResultDataSite;
import model.SiteMap;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class ResultDataSiteServiceImpl implements ResultDataSiteService {


    private HashMap<String, ArrayList<String>> getTagsText(String url, ArrayList<String> tags) throws IOException {
        HashMap<String, ArrayList<String>> tagsText = new HashMap<>();
        Document doc = HttpDao.getDocument(url);
        for (String tag: tags) {
            Elements elements = doc.getElementsByTag(tag);
            ArrayList<String> tmlList = new ArrayList<>();
            for (Element el : elements) {
                tmlList.add(el.getElementsByTag(tag).text());
                tagsText.put(tag, tmlList);
            }
        }

        return tagsText;
    }

    @Override
    public ResultDataSite getResultDataSite(SiteMap siteMap) throws IOException {
        HashMap<String, HashMap<String, ArrayList<String>>> tagsTextMap = new HashMap<>();
        for (String url : siteMap.getUrl()) {
            tagsTextMap.put(url, getTagsText(url, siteMap.getTags()));
        }
        return new ResultDataSite(tagsTextMap);
    }
}
