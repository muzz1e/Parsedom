package service.sitemap.resultdatasite;

import model.ResultDataSite;
import model.SiteMap;

import java.io.IOException;

public interface ResultDataSiteService {

    ResultDataSite getResultDataSite (SiteMap siteMap) throws IOException;
}
