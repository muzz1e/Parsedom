package service.sitemap;

import model.SiteMap;

import java.util.ArrayList;

public interface SiteMapService {

    SiteMap getUrlsByXml(String fileName, ArrayList<String> tags);
}
