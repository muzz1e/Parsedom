package model;

import java.util.ArrayList;
import java.util.HashMap;

public class ResultDataSite {
    private HashMap<String, HashMap<String, ArrayList<String>>> tagsTextMap;

    public ResultDataSite(HashMap<String, HashMap<String, ArrayList<String>>>  tagsTextMap) {
        this.tagsTextMap = tagsTextMap;
    }

    public HashMap<String, HashMap<String, ArrayList<String>>>  getTagsTextMap() {
        return tagsTextMap;
    }
}
