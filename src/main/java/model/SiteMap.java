package model;

import java.util.ArrayList;

public class SiteMap {

    ArrayList<String> url;
    ArrayList<String> tags;

    public SiteMap(ArrayList<String> url ,ArrayList<String> tag) {
        this.url = url;
        this.tags = tag;
    }

    public ArrayList<String> getUrl() {
        return url;
    }

    public ArrayList<String> getTags() {
        return tags;
    }
}
