import dao.ExcelDao;
import dao.FilesDao;
import model.ResultDataSite;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import service.sitemap.SiteMapServiceImpl;
import service.sitemap.resultdatasite.ResultDataSiteServiceImpl;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) throws IOException {
        

        System.out.println(new Date());
        ArrayList<String> tags = new ArrayList<>();
        tags.add("h1");
        tags.add("h2");
        tags.add("h3");
        SiteMapServiceImpl siteMapService = new SiteMapServiceImpl();
        ResultDataSiteServiceImpl resultDataSiteService = new ResultDataSiteServiceImpl();
        ResultDataSite resultDataSite = resultDataSiteService.getResultDataSite(siteMapService.getUrlsByXml("C:\\1\\sitemap.xml", tags));
        ExcelDao.writeResult("C:\\1\\test.xls", resultDataSite.getTagsTextMap());
        System.out.println(new Date());

//
//
//



    }

    private static HashMap<String, ArrayList<String>> getTestMap (){
        HashMap<String, ArrayList<String>> hashMap = new HashMap<>();
        ArrayList<String> list = new ArrayList<>();
        list.add("test1");
        list.add("test2");
        list.add("test3");
        hashMap.put("urlTest", list);
        return hashMap;
    }

}
